require("dotenv").config();
const validator = require("validator");
const axios = require("axios");
const url = process.env.URL;
const { validateDepartment } = require("./departmentChecker");

module.exports = {
  //get all agents
  getAllAgent: async () => {
    try {
      const resp = await axios.get(`${url}/Agent`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      return {
        status: "Failed",
        error: err,
      };
    }
  },

  //Setting status of user to agent status
  setUserIsAgent: async (id) => {
    try {
      const resp = await axios.put(`${url}/User/${id}/makeAgent`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
      //  console.log(resp);
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  //Adding new agent
  addNewAgent: async (agent) => {
    try {
      if (agent.user_id) {
        const resp = await axios.post(`${url}/Agent`, agent);
        if (resp && resp.data) {
          return {
            status: "Success",
            data: `Agent Has been added Successfully`,
          };
        }
      } else {
        return {
          status: "Failed",
          error: `Please give the user id`,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  //Returning all the tickets of an agent
  getAllTicketOfAgent: async (id) => {
    try {
      const resp = await axios.get(`${url}/Agent/${id}/ticket`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  // Adds Agent Comment to a Ticket
  addAgentCommentToTicket: async (agent_id, ticket_id, agentComment) => {
    try {
      if (agentComment.comment && agent_id && ticket_id) {
        const resp = await axios.put(
          `${url}/Agent/${agent_id}/ticket/${ticket_id}/comment`,
          agentComment
        );
        console.log(resp);
        if (resp && resp.data) {
          return {
            status: "Success",
            data: `Comment Has been added Successfully`,
          };
        }
      } else {
        return {
          status: "Failed",
          error: `Please give the Agent id`,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },
};
