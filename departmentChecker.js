let department = [
  "App Development",
  "BFSI",
  "Big Data & EDW",
  "BMC",
  "Business Intelligence",
  "Cloud Infra & Security",
  "Cloud Infrastructure",
  "Cloud Innovation",
  "Data Integration",
  "Data Science",
  "DevOps and App Modernization",
  "Engineering-D365 and Power Apps",
  "Engineering-PMO/QA",
  "Enterprise Analytics",
  "Executive Management",
  "Finance",
  "HR-IT Admin",
  "Java & E-Commerce",
  "Learning & Development",
  "Legal",
  "Management",
  "Marketing",
  "MWP & Chatbot",
  "PMO",
  "Sales",
  "Solution Sales",
];

module.exports = {
  validateDepartment: (dept) => {
    if (department.includes(dept)) {
      return true;
    } else {
      return false;
    }
  },
};
