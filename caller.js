const {
  getAllUser,
  getUserById,
  addNewUser,
  getUserAllTicket,
  addTicket,
  getAllCommentsOfTicket,
  addCommentToTicket,
  changeTicketState,
} = require("./apiFunctions");

const {
  setUserIsAgent,
  addNewAgent,
  getAllTicketOfAgent,
  addAgentCommentToTicket,
  getAllAgent,
} = require("./agentFunctions");

//Function to Call Get All User
const callGetAllUser = async () => {
  const data = await getAllUser();
  // console.log(data);
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};
//Function to Call Get User By Id
const callGetUserById = async (id) => {
  const data = await getUserById(id);
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};
//Function to Call Add New User
const callAddNewUser = async () => {
  const data = await addNewUser({
    first_name: "Thomas",
    last_name: "shelby",
    email: "ss@gmail.com",
    department: "IT",
  });

  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Get User All Ticket
const callgetUserAllTicket = async (id) => {
  let userallticket = [];
  let userticket = {};
  const data = await getUserAllTicket(id);
  if (data.status == "Success") {
    const allTickets = data.data.data.ticketTables;
    // console.log(allTickets);
    allTickets.forEach((ticket) => {
      // console.log('Ticket',ticket);
      userticket.title = ticket.ticket.title;
      userticket.description = ticket.ticket.description;
      userticket.issue_date = ticket.ticket.issue_observed_on;
      userallticket.push(userticket);
    });
    console.log(userallticket);
  } else {
    console.log(data.error);
  }
};

//Add Ticket
const callAddTicket = async () => {
  const ticket = await addTicket({
    title: "Probelm 1",
    department: "Finance",
    description: "Ticket Description 3",
    issue_observed_on: "2021-10-11",
    urgency: "2",
    impact: "1",
    caller: "USR10",
  });
  if (ticket.status == "Success") {
    console.log(ticket.data);
  } else {
    console.log(ticket.error);
  }
};

//Set User As Agent
const callSetUserIsAgent = async (id) => {
  const data = await setUserIsAgent(id);
  //console.log(data);
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Add New Agent
const callAddNewAgent = async () => {
  const data = await addNewAgent({
    user_id: "USR9",
  });

  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Return Comments of a ticket
const callGetCommentsOfTicket = async (id) => {
  const data = await getAllCommentsOfTicket(id);
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Adds USer comment to a ticket
const callAddCommentToTicket = async () => {
  const data = await addCommentToTicket("USR1", "TICK2", {
    comment: "Probelm 1",
  });
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Return all tickets of an agent
const callGetTicketOfAgent = async (id) => {
  const data = await getAllTicketOfAgent(id);
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Change Ticket State
const callChangeTicketState = async () => {
  const data = await changeTicketState("AGE3", "TICK6", {
    ticket_state: 2,
  });
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//Add Agent Comment To Ticket
const callAddAgentCommentToTicket = async () => {
  const data = await addAgentCommentToTicket("AGE1", "TICK5", {
    comment: "Testing",
  });
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//get all agents
const callGetAllAgent = async () => {
  const data = await getAllAgent();
  if (data.status == "Success") {
    console.log(data.data);
  } else {
    console.log(data.error);
  }
};

//callGetAllUser();
//callGetUserById("USR100");
//callAddNewUser();
//callgetUserAllTicket("USR1");
//callAddTicket();
//callSetUserIsAgent("USR10");
//callAddNewAgent();
callGetCommentsOfTicket("TICK27");
//callAddCommentToTicket();
//callGetTicketOfAgent("AGE1");
//callChangeTicketState();
//callAddAgentCommentToTicket();
//callGetAllAgent();
