require("dotenv").config();
const validator = require("validator");
const axios = require("axios");
const url = process.env.URL;
const { validateDepartment } = require("./departmentChecker");

module.exports = {
  getAllUser: async () => {
    try {
      const resp = await axios.get(`${url}/User`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      return {
        status: "Failed",
        error: err,
      };
    }
  },

  getUserById: async (id) => {
    try {
      const resp = await axios.get(`${url}/User/${id}`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  addNewUser: async (user) => {
    try {
      if (user.first_name && user.last_name && user.email && user.department) {
        const resp = await axios.post(`${url}/User`, user);
        if (resp && resp.data) {
          return {
            status: "Success",
            data: `User Has been added Successfully`,
          };
        }
      } else {
        return {
          status: "Failed",
          error: `Please Fill All the Details`,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  getUserAllTicket: async (id) => {
    try {
      const resp = await axios.get(`${url}/User/${id}/allTickets`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  addTicket: async (ticket) => {
    try {
      if (
        ticket.title &&
        ticket.department &&
        ticket.description &&
        ticket.issue_observed_on &&
        ticket.urgency &&
        ticket.impact &&
        ticket.caller
      ) {
        const departmentValidate = validateDepartment(ticket.department);
        if (departmentValidate) {
          const resp = await axios.post(`${url}/Ticket`, ticket);
          if (resp && resp.data) {
            return {
              status: "Success",
              data: `Ticket Has been added Successfully`,
            };
          }
        } else {
          return {
            status: "Failed",
            error: `Please Enter a valid department`,
          };
        }
      } else {
        return {
          status: "Failed",
          error: `Please Fill All the Details`,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  //Return all the comments of a ticket
  getAllCommentsOfTicket: async (id) => {
    try {
      const resp = await axios.get(`${url}/Ticket/${id}/comments`);
      if (resp && resp.data) {
        return {
          status: "Success",
          data: resp.data,
        };
      }
    } catch (err) {
      return {
        status: "Failed",
        error: err,
      };
    }
  },

  //Adds a comment to a ticket
  addCommentToTicket: async (user_id, ticket_id, userComment) => {
    try {
      if (userComment.comment && user_id && ticket_id) {
        const resp = await axios.put(
          `${url}/User/${user_id}/ticket/${ticket_id}/comment`,
          userComment
        );
        console.log(resp);
        if (resp && resp.data) {
          return {
            status: "Success",
            data: `Comment Has been added Successfully`,
          };
        }
      } else {
        return {
          status: "Failed",
          error: `Please give the user id`,
        };
      }
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },

  //Change Ticket State
  changeTicketState: async (agent_id, ticket_id, ticket_state) => {
    try {
      if ((agent_id, ticket_id)) {
        const resp = await axios.put(
          `${url}/Agent/${agent_id}/ticket/${ticket_id}`,
          ticket_state
        );
        if (resp && resp.data) {
          return {
            status: "Success",
            data: resp.data,
          };
        } else {
          return {
            status: "Failed",
            error: err.response.data,
          };
        }
      }
      //  console.log(resp);
    } catch (err) {
      if (err.response) {
        return {
          status: "Failed",
          error: err.response.data,
        };
      }
    }
  },
};
